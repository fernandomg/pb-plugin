1// API definition for Restaurant App plugin.

"use strict";

var stringToArrayBuffer = function(str) {
    var ret = new Uint8Array(str.length);
    for (var i = 0; i < str.length; i++) {
        ret[i] = str.charCodeAt(i);
    }
    // TODO would it be better to return Uint8Array?
    return ret.buffer;
};

var base64ToArrayBuffer = function(b64) {
    return stringToArrayBuffer(atob(b64));
};

function massageMessageNativeToJs(message) {
    if (message.CDVType == 'ArrayBuffer') {
        message = base64ToArrayBuffer(message.data);
    }
    return message;
}

// Cordova 3.6 doesn't unwrap ArrayBuffers in nested data structures
// https://github.com/apache/cordova-js/blob/94291706945c42fd47fa632ed30f5eb811080e95/src/ios/exec.js#L107-L122
function convertToNativeJS(object) {
    Object.keys(object).forEach(function (key) {
        var value = object[key];
        object[key] = massageMessageNativeToJs(value);
        if (typeof(value) === 'object') {
            convertToNativeJS(value);
        }
    });
}

module.exports = {

    scan: function (services, seconds, success, failure) {
        var successWrapper = function(peripheral) {
            convertToNativeJS(peripheral);
            success(peripheral);
        };
        cordova.exec(successWrapper, failure, 'RestaurantPlugin', 'scan', [services, seconds]);
    },

    startScan: function (success, failure) {
        var successWrapper = function(peripheral) {
            convertToNativeJS(peripheral);
            success(peripheral);
        };
        cordova.exec(success, failure, 'RestaurantPlugin', 'startScan', [2000,0,10]);
    },
	stopScan: function (success, failure) {
        var successWrapper = function(peripheral) {
            convertToNativeJS(peripheral);
            success(peripheral);
        };
		cordova.exec(success, failure, 'RestaurantPlugin', 'stopScan', []);
    },

	startSimulatedScan: function (success, failure) {
        var successWrapper = function(peripheral) {
            convertToNativeJS(peripheral);
            success(peripheral);
        };
        cordova.exec(success, failure, 'RestaurantPlugin', 'startSimulatedScan', [2000,4000,'simulator-default.csv']);
    },

	stopSimulatedScan: function (success, failure) {
        var successWrapper = function(peripheral) {
            convertToNativeJS(peripheral);
            success(peripheral);
        };
		cordova.exec(success, failure, 'RestaurantPlugin', 'stopSimulatedScan', []);
    },

    list: function (success, failure) {
        cordova.exec(success, failure, 'RestaurantPlugin', 'list', []);
    },

    connect: function (device_id, success, failure) {
        var successWrapper = function(peripheral) {
            convertToNativeJS(peripheral);
            success(peripheral);
        };
        cordova.exec(successWrapper, failure, 'RestaurantPlugin', 'connect', [device_id]);
    },

    disconnect: function (device_id, success, failure) {
        cordova.exec(success, failure, 'RestaurantPlugin', 'disconnect', [device_id]);
    },

	getBagTagSummary: function (services, seconds, success, failure) {
        var successWrapper = function(peripheral) {
            convertToNativeJS(peripheral);
            success(peripheral);
        };
        cordova.exec(successWrapper, failure, 'RestaurantPlugin', 'getBagTagSummary', ['TACC001','02010603160A1807094261675461671DFFFFFF01BB48EB5E2342CB01011100AA08DC08A81683005D00A1B6C455']);
    },

	getBagTagSummaryTest: function (services, seconds, success, failure) {
        var successWrapper = function(peripheral) {
            convertToNativeJS(peripheral);
            success(peripheral);
        };
        cordova.exec(successWrapper, failure, 'RestaurantPlugin', 'getBagTagSummaryTest', ['TACC001','22.18']);
    },
	setBagTagTempRange: function (services, seconds, success, failure) {
        var successWrapper = function(peripheral) {
            convertToNativeJS(peripheral);
            success(peripheral);
        };
        cordova.exec(successWrapper, failure, 'RestaurantPlugin', 'setBagTagTempRange', [['00:43:A8:23:10:F0','00:43:A8:23:10:F1'],'25.00','40.00','2000']);
    },

	setBagTagMode: function (bagMode, success, failure) {
    var successWrapper = function (peripheral) {
      convertToNativeJS(peripheral);
      success(peripheral);
    };
    cordova.exec(successWrapper, failure, 'RestaurantPlugin', 'setBagTagMode', []);
  }


};
