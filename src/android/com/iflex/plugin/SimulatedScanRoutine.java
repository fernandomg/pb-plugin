package com.iflex.plugin;

import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

import com.iflex.api.bluetooth.IflexBluetoothTag;
import com.iflex.api.bluetooth.Obj2JSON;
import com.iflex.api.bluetooth.androidcomm.IflexBluetoothReader;
import com.iflex.api.bluetooth.IflexBluetoothAdvertisementSummary;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import au.com.bytecode.opencsv.CSVReader;

public class SimulatedScanRoutine {

	String DEFAULT_SIMULATOR_FILE = "www/apps/pb/RestaurantApp/test/simulator-default.csv";
	// these variables need to be declared in your own class.
	private ScanRoutine mRunnable;

	List<BagTagDataDTO> rows = new ArrayList<BagTagDataDTO>();

	public ScanRoutine getmRunnable() {
		return mRunnable;
	}

	public boolean isScanRunning() {
		return mRunnable != null;
	}

	IflexBluetoothReader mIflexReader;

	CallbackContext callbackContext;
	PluginResult result;

	Integer maxTime;

	public Integer getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(Integer maxTime) {
		this.maxTime = maxTime;
	}

	// constructor initialized the above declared variables.
	public SimulatedScanRoutine(IflexBluetoothReader mIflexReader,
			Integer scanPeriodMilliseconds, Integer sleepPeriodMilliseconds,
			CallbackContext discoverCallback, PluginResult result,
			AssetManager assetManager, String fileName) {

		Log.d("SSR", "SSR Init");
		this.callbackContext = discoverCallback;
		this.mIflexReader = mIflexReader;

		if (mRunnable == null) {
			Log.d("SSR", "SSR mRunnable - null");
			// spawns new thread for runnable, runnable will continue to
			// broadcast until the BluetoothRunnable.CancelRunnable() is called
			this.mRunnable = new ScanRoutine(this);
			ChangeScanPeriod(scanPeriodMilliseconds);
			ChangeSleepPeriod(sleepPeriodMilliseconds);
			parseCSV(assetManager, fileName);
			Log.d("SSR", "SSR before new Thread");
			new Thread(this.mRunnable).start();

		}

	}

	public void parseCSV(AssetManager assetManager, String fileName) {

		try {
			// TODO: add file url
			InputStream input = null;
			Log.d("parseCSV", DEFAULT_SIMULATOR_FILE);
			if (fileName == null) {
				input = assetManager.open(DEFAULT_SIMULATOR_FILE);
			} else {
				// Look for the file in downloads directory
				File file = new File(Environment
						.getExternalStoragePublicDirectory(
								Environment.DIRECTORY_DOWNLOADS)
						.getAbsolutePath(), fileName);
				if (file.exists()) {
					input = new FileInputStream(file);
				} else {
					input = assetManager.open(DEFAULT_SIMULATOR_FILE);
				}
			}
			Log.d("parseCSV", ("input file is null == " + (input == null)));
			CSVReader csvReader = new CSVReader(new InputStreamReader(input));

			String[] row = null;

			row = csvReader.readNext(); // Ignore headers
			while ((row = csvReader.readNext()) != null) {
				Log.d("parseCSV", "setTime - BTID: " + row[0] + " - " + row[1]);
				BagTagDataDTO dto = new BagTagDataDTO();
				
				try {
					dto.setTime(Integer.valueOf(row[0]));
				}
				catch (Exception e) {
					Log.e("parseCSV", "error parsing row " + Arrays.toString(row)); 
					continue;
				}
				
				dto.setBtid(row[1]);
				
				dto.setMainTemp(parseFloatNumber(row[2]));

				dto.setRemoteTemp(parseFloatNumber(row[3]));
				dto.setRemoteHumidity(parseFloatNumber(row[4]));
				dto.setRemoteLight(parseFloatNumber(row[5]));
				dto.setMealType(row[6]);
				dto.setMode(row[7]);
				dto.setOpen(row[8]);
				dto.setPlugState(row[9]);
				dto.setFanState(row[10]);
				dto.setRemoteSensor(row[11]);
				dto.setHeaterState(row[12]);
				dto.setLidState(row[13]);
				dto.setMainBatery(parseFloatNumber(row[14]));
				
				rows.add(dto);

			}

			
			Collections.sort(rows);
			maxTime = rows.get(rows.size() - 1).getTime();
			csvReader.close();

		} catch (IOException e) {
			e.printStackTrace();
			String message = "Error parsing csv";

			throw new RuntimeException(message);
		}

	}
	
	private Float parseFloatNumber(final String n) {
		try {
			return Float.valueOf(n);
		}
		catch (Exception e) {
			return null;
		}
	}

	// Changes how long the scanner will scan for.
	public void ChangeScanPeriod(int scanPeriodmilliseconds) {
		if (mRunnable != null) {
			this.mRunnable.SCAN_PERIOD_MS = scanPeriodmilliseconds;
		}
	}

	public void ChangeSleepPeriod(int sleepPeriodmilliseconds) {
		if (mRunnable != null) {
			this.mRunnable.SLEEP_PERIOD_MS = sleepPeriodmilliseconds;
		}
	}

	public class ScanRoutine implements Runnable {
		private volatile boolean IsCancelled = false;
		private Integer SCAN_PERIOD_MS; // BT STACK WILL SCAN FOR THIS LONG
		// (Should be tuned later)
		private Integer SLEEP_PERIOD_MS; // BT STACK WILL SLEEP FOR THIS LONG
											// (should be tuned later NEEDED FOR
											// PROCESSING AND BATT LIFE)
		private Thread currentThread;
		private SimulatedScanRoutine simulatedScanroutine;

		private int timeCounter = 0;

		ScanRoutine(SimulatedScanRoutine c) {
			this.simulatedScanroutine = c;

		}

		public synchronized void CancelRunnable() {
			this.IsCancelled = true;
			currentThread.interrupt();
		}

		void WakeUpFromSleep() {
			this.currentThread.interrupt();
		}

		@Override
		public void run() {

			android.os.Process
					.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
			this.currentThread = Thread.currentThread();
			Set<String> advertisedTags = new HashSet<String>();
			while (true) {
				
				if (this.IsCancelled || timeCounter > simulatedScanroutine.getMaxTime()) {
					break;
				}
				timeCounter += SCAN_PERIOD_MS / 1000;

				List<BagTagDataDTO> availableRows = getAvailableRows();

				// Send last information
				Collections.reverse(availableRows);

				for (BagTagDataDTO r : availableRows) {

					if (!advertisedTags.contains(r.getBtid())) {
						generateBagSummary(r);
						advertisedTags.add(r.getBtid());
					}
					r.setProcessed(true);

				}
				advertisedTags.clear();
				try {
					Thread.sleep(SCAN_PERIOD_MS);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				timeCounter += SLEEP_PERIOD_MS / 1000;

				try {
					Thread.sleep(SLEEP_PERIOD_MS);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}

		private List<BagTagDataDTO> getAvailableRows() {
			List<BagTagDataDTO> availableRows = new ArrayList<BagTagDataDTO>();
			for (BagTagDataDTO r : rows) {
				if (!r.isProcessed() && r.getTime().compareTo(timeCounter) <= 0) {
					availableRows.add(r);
				}

			}
			return availableRows;
		}
	}

	private void generateBagSummary(BagTagDataDTO row) {

		try {

			short statusBits = generateStatusBits(row);

			Date now = new Date();
			IflexBluetoothTag.TAG_STATE tagState = (row.getMode()
					.equalsIgnoreCase("RUNNING")) ? IflexBluetoothTag.TAG_STATE.Running
					: IflexBluetoothTag.TAG_STATE.Invalid;
			IflexBluetoothTag newTag = new IflexBluetoothTag(row.getBtid(),
					11.0, 11, row.getBtid());
			
			IflexBluetoothAdvertisementSummary sum = new IflexBluetoothAdvertisementSummary(
					now.getTime() / 1000, row.getMainTemp(),
					row.getRemoteTemp(), row.getRemoteHumidity(),
					row.getRemoteLight(), row.getMainBateryInt(),
					100, statusBits, tagState);

			newTag.SetLastTagSummary(sum);

			String jsonMessage = Obj2JSON.GenerateBagTagSummaryJson(newTag,
					mIflexReader);

			if (jsonMessage != null) {
				System.out.print("Succesfully created a BagTagSummaryJson "
						+ jsonMessage);
				JSONObject json = new JSONObject(jsonMessage);

				result = new PluginResult(PluginResult.Status.OK, json);

			} else {
				result = new PluginResult(PluginResult.Status.ERROR,
						"Could not genereate message");
			}

		} catch (Exception e) {
			e.printStackTrace();
			result = new PluginResult(PluginResult.Status.ERROR,
					"Could not genereate message");
		}
		result.setKeepCallback(true);
		callbackContext.sendPluginResult(result);
	}

	private short generateStatusBits(BagTagDataDTO row) {
		short fan = 0x4000;
		short heaterOn = 0x2000;
		short powerAttach = 0x1000;
		short open = 0x20;
		short remoteSensorAttach = 0x10;
		short mealType = 0x2;
		short bleConected = 0x1;

		short status = 0;
		if (row.getFanState().equalsIgnoreCase("On")) {
			status += fan;
		}
		if (row.getHeaterState().equalsIgnoreCase("On")) {
			status += heaterOn;
		}
		if (row.getPlugState().equalsIgnoreCase("On")) {
			status += powerAttach;
		}
		if (row.getOpen().equalsIgnoreCase("Yes")) {
			status += open;
		}
		if (row.getRemoteSensor().equalsIgnoreCase("PRESENT")) {
			status += remoteSensorAttach;
		}
		if (row.getMealType().equalsIgnoreCase("HOT")) {
			status += mealType;
		}

		status += bleConected;

		return status;
	}
}
