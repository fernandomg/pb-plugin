package com.iflex.plugin;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.content.res.AssetManager;
import android.text.TextUtils;
import android.util.Log;

import com.iflex.api.bluetooth.IflexBluetoothAdvertisementSummary;
import com.iflex.api.bluetooth.IflexBluetoothTag;
import com.iflex.api.bluetooth.Obj2JSON;
import com.iflex.api.bluetooth.ParseBluetoothAdvertisementResponse;
import com.iflex.api.bluetooth.ResponseCallback;
import com.iflex.api.bluetooth.TagParser;
import com.iflex.api.bluetooth.androidcomm.IflexBluetoothReader;

/**
 * This class provide the methods to get the information from BLE devices
 */
@SuppressLint("DefaultLocale")
public class RestaurantPlugin extends CordovaPlugin {

	// actions
	private static final String START_SCAN = "startScan";
	private static final String STOP_SCAN = "stopScan";

	private static final String START_SIMULATED_SCAN = "startSimulatedScan";
	private static final String STOP_SIMULATED_SCAN = "stopSimulatedScan";

	private static final String GET_BAG_TAG_SUMMARY = "getBagTagSummary";
	private static final String GET_BAG_TAG_SUMMARY_TEST = "getBagTagSummaryTest";

	private static final String SET_BAG_TAGS_TEMP_RANGE = "setBagTagTempRange";
	private static final String SET_BAG_TAGS_MODE = "setBagTagMode";

	private static final String TAG = "RestaurantPlugin";

	RestaurantAPPAdvertiseScanCallback cb;

	SimulatedScanRoutine simulatedScanRoutine;

	IflexBluetoothReader mIflexReader;

	public RestaurantAPPAdvertiseScanCallback getCb(
			IflexBluetoothReader mIflexReader, Integer scanPeriodMilliseconds,
			Integer sleepPeriodMilliseconds, Integer timeoutScanSeconds,
			CallbackContext callbackContext, PluginResult result) {

		if (cb == null) {
			cb = new RestaurantAPPAdvertiseScanCallback(mIflexReader,
					scanPeriodMilliseconds, sleepPeriodMilliseconds,
					timeoutScanSeconds, callbackContext, result);
		}
		return cb;
	}

	public SimulatedScanRoutine getSimulatedScanRoutine(
			IflexBluetoothReader mIflexReader, Integer scanPeriodMilliseconds,
			Integer sleepPeriodMilliseconds, CallbackContext callbackContext,
			PluginResult result, AssetManager assetManager, String fileName) {

		if (simulatedScanRoutine == null) {
			Log.d("Scan Routine", "Simulated Scan Routine");
			simulatedScanRoutine = new SimulatedScanRoutine(mIflexReader,
					scanPeriodMilliseconds, sleepPeriodMilliseconds,
					callbackContext, result, assetManager, fileName);
		}

		return simulatedScanRoutine;
	}

	public boolean isScanRunning() {
		return cb != null && cb.isScanRunning();
	}

	public boolean isSimulatedScanRunning() {
		return simulatedScanRoutine != null
				&& simulatedScanRoutine.isScanRunning();
	}

	protected void pluginInitialize() {

		BluetoothAdapter bluetoothAdapter = BluetoothAdapter
				.getDefaultAdapter();
		if (bluetoothAdapter == null) {
			Log.d(TAG, "Couldn't created a bluetoothAdapter instance");
		}

		// TODO: remove harcoded serialNumber and device id
		mIflexReader = new IflexBluetoothReader("4656", bluetoothAdapter,
				cordova.getActivity().getBaseContext());
		mIflexReader.setDeviceID("666");
		mIflexReader.setGPSStatus(true);
		mIflexReader.SetGPSCoordinates(35.850862, -78.886049);
	}

	@Override
	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) throws JSONException {
		
		Log.d(TAG, "action = " + action);

		boolean validAction = true;

		if (action.equals(GET_BAG_TAG_SUMMARY_TEST)) {

			String epc = args.getString(0);
			Double mainTemp = args.getDouble(1);

			Integer mainTempInteger = (int) Math.round(mainTemp * 100);

			String mainTempHex = Integer.toHexString(mainTempInteger);

			if (mainTempHex.length() == 3) {
				mainTempHex = "0" + mainTempHex;
			}

			List<String> str = (Arrays
					.asList(mainTempHex.split("(?<=\\G.{2})")));
			Collections.reverse(str);

			String mainTempReplacement = TextUtils.join("", str).toUpperCase();

			String rawBLE = "02010603160A1807094261675461671DFFFFFF01BB48EB5E2342CB01011100{main-temp}DC08A81683005D00A1B6C455";
			rawBLE = rawBLE.replaceAll("\\{main-temp\\}", mainTempReplacement);

			ParseBluetoothAdvertisementResponse pBAR = TagParser
					.ParseBluetoothAdvertisement(epc, 1, rawBLE);

			if (pBAR.commandSucceeded()) {
				IflexBluetoothTag parsedTag = pBAR.getBluetoothTag();

				PluginResult result = null;

				if (parsedTag != null) {

					try {

						String jsonMessage = Obj2JSON
								.GenerateBagTagSummaryJson(parsedTag,
										mIflexReader);

						if (jsonMessage != null) {
							System.out
									.print("SUCCESFULLY CREATED A BAGTAGSUMMARYJSON"
											+ jsonMessage);
							JSONObject json = new JSONObject(jsonMessage);

							result = new PluginResult(PluginResult.Status.OK,
									json);

						} else {
							result = new PluginResult(
									PluginResult.Status.ERROR,
									"COULD NOT GENEREATE MESSAGE");
						}

					} catch (Exception e) {
						result = new PluginResult(PluginResult.Status.ERROR,
								"COULD NOT GENEREATE MESSAGE");
					}
				} else {

					result = new PluginResult(PluginResult.Status.ERROR,
							"COULD NOT PARSE TAG");
				}

				callbackContext.sendPluginResult(result);
			}
		} else if (action.equals(SET_BAG_TAGS_TEMP_RANGE)) {

			PluginResult result = null;

			JSONArray bagIds = args.getJSONArray(0);

			Float minTemp = Float.valueOf(args.getString(1));
			Float maxTemp = Float.valueOf(args.getString(2));
			Integer timeout = args.getInt(3);

			for (int i = 0; i < bagIds.length(); i++) {
				ResponseCallback rc = null;

				IflexBluetoothTag t = new IflexBluetoothTag(
						bagIds.getString(i), 11.0D, 11, bagIds.getString(i));

				mIflexReader
						.AsyncSetTempRange(t, minTemp, maxTemp, rc, timeout);

			}

			result = new PluginResult(PluginResult.Status.OK,
					"BAG TAGS TEMP RANGE SET CORRECTLY");

			callbackContext.sendPluginResult(result);

		} else if (action.equals(START_SCAN)) {
			PluginResult result = null;

			Integer scanPeriodMilliseconds = (Integer) args.get(0); // BT STACK
																	// WILL SCAN
																	// FOR THIS
																	// LONG

			Integer sleepPeriodMilliseconds = (Integer) args.get(1);

			Integer timeoutScanSeconds = (Integer) args.get(2);

			if (!isScanRunning()) {
				getCb(mIflexReader, scanPeriodMilliseconds,
						sleepPeriodMilliseconds, timeoutScanSeconds,
						callbackContext, result);
				result = new PluginResult(PluginResult.Status.OK,
						"SCAN STARTED");
			} else {
				result = new PluginResult(PluginResult.Status.ERROR,
						"SCAN IS ALREADY RUNNING");

			}
			result.setKeepCallback(true);
			callbackContext.sendPluginResult(result);

		} else if (action.equals(STOP_SCAN)) {

			PluginResult result;
			try {

				if (isScanRunning()) {
					cb.getmRunnable().CancelRunnable();
					cb = null;
					result = new PluginResult(PluginResult.Status.OK,
							"SCAN HAS STOPPED");

				} else {
					Log.d("Scan", "SCAN WAS NOT RUNNING");
					result = new PluginResult(PluginResult.Status.ERROR,
							"SCAN WAS NOT RUNNING");
				}
			} catch (Exception e) {
				e.printStackTrace();
				result = new PluginResult(PluginResult.Status.ERROR,
						"ERROR STOPPOING SCAN");

			}
			result.setKeepCallback(true);
			callbackContext.sendPluginResult(result);

		} else if (action.equals(START_SIMULATED_SCAN)) {
			PluginResult result = null;

			Integer scanPeriodMilliseconds = (Integer) args.get(0); // BT STACK
			// WILL SCAN
			// FOR THIS
			// LONG

			Integer sleepPeriodMilliseconds = (Integer) args.get(1);

			String fileName = args.length() > 2 ? args.getString(2) : null;
			// args.getString(2);

			if (!isSimulatedScanRunning()) {

				Log.d("Scan Started", "Scan Started 01");
				getSimulatedScanRoutine(mIflexReader, scanPeriodMilliseconds,
						sleepPeriodMilliseconds, callbackContext, result,
						cordova.getActivity().getBaseContext().getAssets(),
						fileName);
				result = new PluginResult(PluginResult.Status.OK,
						"SCAN STARTED");
				Log.d("Scan Started", "Scan Started 02");
			} else {
				result = new PluginResult(PluginResult.Status.ERROR,
						"SCAN IS ALREADY RUNNING");

			}
			result.setKeepCallback(true);
			callbackContext.sendPluginResult(result);

		} else if (action.equals(STOP_SIMULATED_SCAN)) {

			PluginResult result;
			try {

				if (isSimulatedScanRunning()) {
					simulatedScanRoutine.getmRunnable().CancelRunnable();
					simulatedScanRoutine = null;
					result = new PluginResult(PluginResult.Status.OK,
							"SCAN HAS STOPPED");

				} else {

					result = new PluginResult(PluginResult.Status.ERROR,
							"SCAN WAS NOT RUNNING");
				}
			} catch (Exception e) {
				e.printStackTrace();
				result = new PluginResult(PluginResult.Status.ERROR,
						"ERROR STOPPING SCAN");

			}
			result.setKeepCallback(true);
			callbackContext.sendPluginResult(result);

		} else if (action.equals(SET_BAG_TAGS_MODE)) {
			String bagModeToSet = args.getString(0);

			PluginResult result = new PluginResult(PluginResult.Status.OK,
					"Called routine to set the bag mode to " + bagModeToSet);
			callbackContext.sendPluginResult(result);
		} else {
			validAction = false;
		}

		return validAction;
	}
}
