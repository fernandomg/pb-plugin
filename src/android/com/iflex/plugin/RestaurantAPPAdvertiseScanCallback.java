package com.iflex.plugin;

import android.bluetooth.BluetoothAdapter;
import android.util.Log;
import com.iflex.api.bluetooth.IflexBluetoothTag;
import com.iflex.api.bluetooth.Obj2JSON;
import com.iflex.api.bluetooth.Utilities;
import com.iflex.api.bluetooth.androidcomm.IflexBluetoothReader;
import com.iflex.api.bluetooth.androidcomm.IflexBluetoothReader.AdvertiseScanCallback;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.json.JSONObject;

import java.util.LinkedHashMap;

public class RestaurantAPPAdvertiseScanCallback implements AdvertiseScanCallback {

	// these variables need to be declared in your own class.
	private BluetoothRunnable mRunnable;

	public BluetoothRunnable getmRunnable() {
		return mRunnable;
	}

	public boolean isScanRunning() {
		return mRunnable != null;
	}

	private BluetoothAdapter mBluetoothAdapter;
	private IflexBluetoothReader mIflexReader;
	private LinkedHashMap<String, IflexBluetoothTag> TagMap;


	private int timeoutInSeconds; // time period for which to ignore duplicate
	// BLE adv. If time period has expired send
	// app the adv.


	CallbackContext callbackContext;
	PluginResult result;

	// constructor initialized the above declared variables.
	public RestaurantAPPAdvertiseScanCallback(IflexBluetoothReader mIflexReader, Integer scanPeriodMilliseconds, Integer sleepPeriodMilliseconds, Integer timeoutScanSec, CallbackContext discoverCallback, PluginResult result) {

		this.callbackContext = discoverCallback;
		TagMap = new LinkedHashMap<String, IflexBluetoothTag>();
		this.timeoutInSeconds = timeoutScanSec;

		// Get bluetoothAdapter via the Android API calls.
		this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

		this.mIflexReader = mIflexReader;

		if (mRunnable == null) {
			// spawns new thread for runnable, runnable will continue to
			// broadcast until the BluetoothRunnable.CancelRunnable() is called
			this.mRunnable = new BluetoothRunnable(this);
			ChangeScanPeriod(scanPeriodMilliseconds);
			ChangeSleepPeriod(sleepPeriodMilliseconds);

			new Thread(this.mRunnable).start();
		}
	}

	// Changes how long the scanner will scan for.
	public void ChangeScanPeriod(int scanPeriodmilliseconds) {
		if (mRunnable != null) {
			this.mRunnable.SCAN_PERIOD_MS = scanPeriodmilliseconds;
		}
	}

	public void ChangeSleepPeriod(int sleepPeriodmilliseconds) {
		if (mRunnable != null) {
			this.mRunnable.SLEEP_PERIOD_MS = sleepPeriodmilliseconds;
		}
	}

	public class BluetoothRunnable implements Runnable {
		private volatile boolean IsCancelled = false;
		private int SCAN_PERIOD_MS ; // BT STACK WILL SCAN FOR THIS LONG
		// (Should be tuned later)
		private int SLEEP_PERIOD_MS ; // BT STACK WILL SLEEP FOR THIS LONG
		// (should be tuned later NEEDED FOR
		// PROCESSING AND BATT LIFE)
		private Thread currentThread;
		private AdvertiseScanCallback mAdvScanCallback;

		BluetoothRunnable(AdvertiseScanCallback c) {
			this.mAdvScanCallback = c;

		}

		public synchronized void CancelRunnable() {
			this.IsCancelled = true;
			currentThread.interrupt();
		}

		void WakeUpFromSleep() {
			this.currentThread.interrupt();
		}

		@Override
		public void run() {
			// TODO Auto-generated method stub
			android.os.Process
					.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);
			this.currentThread = Thread.currentThread();
			mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
			if (mBluetoothAdapter == null) {
				Log.d("AsyncTask", "Bluetooth not supported!");
				return;
			} else {
				mBluetoothAdapter.enable();

				int CURRENT_SCAN_INTERVAL;

				while (true) {
					if (this.IsCancelled) {
						break;
					}

					CURRENT_SCAN_INTERVAL = SCAN_PERIOD_MS;

					Log.i("BluetoothRunnable", "starting scan...");

					// This tells the BT stack to start looking for all
					// tags-async

					boolean result = mIflexReader
							.StartAdvScan(this.mAdvScanCallback);
					// START BT STACK SCANNING-now wait for
					// CURRENT_SCAN_INTERVAL
					try {
						Thread.sleep(CURRENT_SCAN_INTERVAL);
					} catch (InterruptedException e) {
					}

					// STOP BT STACK FROM SCANNING
					mIflexReader.StopAdvScan();

					// BT STACK NOT SCANNING-now wait for SLEEP_PERIOD_MS
					try {
						Thread.sleep(SLEEP_PERIOD_MS);
					} catch (InterruptedException e) {
					}
				}
			}

			mIflexReader.StopAdvScan();
			Log.d("doInBackground", "Exiting...");

		}
	}

	// THIS ROUTINE GETS CALL EVERY TIME THE ANDROID BT Stack see a tag adv
	@Override
	public void BluetoothAdvScanCallback(IflexBluetoothTag newTag) {
		if (!TagMap.containsKey(newTag.GetUniqueIdentifier())) {
			// TAG SEEN FOR THE FIRST TIME
			TagMap.put(newTag.GetUniqueIdentifier(), newTag);

			generateBagSummary(newTag,mIflexReader);

			return;
		} else {
			// TAG SEEN EARLIER-Now check if anything changed on the tag

			// Gets shallow copy of the tag, any modifications on the
			// originalTag object
			// SHOULD change the same object in the TagMap LinkedHashMap (please
			// verify in Javascript or other
			// non-Android libraries)
			IflexBluetoothTag originalTag = TagMap.get(newTag
					.GetUniqueIdentifier());

			// if = If the data in the originalTag object is more than
			// timeoutInSeconds old, replace the data in the originalTag
			// object with the data in the newTAg
			if (originalTag.LastSeenAt() + timeoutInSeconds < Utilities
					.GetCurrentUnixEpochSeconds()) {
				// TIMEOUT EXPIRED
				originalTag.MergeRecords(newTag);

				// 440LAB PROCESS EXISTING TAG HERE-TIMER CHECKED FIRST SO TAG
				// STATUS "COULD" HAVE CHANGED
				generateBagSummary(newTag,mIflexReader);

				return;
			}
			// else if = If an event has occurred (such as an opening of a lid),
			// replace the data in the originalTag object
			// with the data in the newTag.
			else if (originalTag
					.EventsOccurred(originalTag.GetLastTagSummary(),
							newTag.GetLastTagSummary())) {
				// LESS THAN TIMEOUT PERIOD
				originalTag.MergeRecords(newTag);

				// 440LAB PROCESS EXISTING TAG HERE-TAG STATUS CHANGED

				generateBagSummary(newTag,mIflexReader);

				return;
			}
		}

	}

	private void generateBagSummary(IflexBluetoothTag newTag,
									IflexBluetoothReader mIflexReader2) {

		try {

			String jsonMessage = Obj2JSON.GenerateBagTagSummaryJson(
					newTag, mIflexReader);

			if (jsonMessage != null) {
				System.out
						.print("Succesfully created a BagTagSummaryJson "
								+ jsonMessage);
				JSONObject json = new JSONObject(jsonMessage);

				result = new PluginResult(PluginResult.Status.OK, json);
			}
			else {
				result = new PluginResult(PluginResult.Status.ERROR, "Could not genereate message");
			}

		} catch (Exception e) {
			result = new PluginResult(PluginResult.Status.ERROR, "Could not genereate message");
		}
		result.setKeepCallback(true);
		callbackContext.sendPluginResult(result);
	}
}
