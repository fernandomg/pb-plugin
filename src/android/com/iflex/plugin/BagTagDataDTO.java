package com.iflex.plugin;


public class BagTagDataDTO  implements Comparable<BagTagDataDTO>{

	 Integer time;
     String btid;
     Float mainTemp;
     Float remoteTemp;
     Float remoteHumidity;
     Float remoteLight;
     String mode;
     String mealType;
     String open;
     String plugState;
     String fanState;
     String remoteSensor;
     String heaterState;
     String lidState;
     Float mainBatery;
     
     boolean processed;
     
	@Override
	public int compareTo(BagTagDataDTO o) {
		// TODO Auto-generated method stub

		return this.getTime().compareTo(o.getTime());
	}

	public Integer getTime() {
		return time;
	}

	public void setTime(Integer time) {
		this.time = time;
	}

	public String getBtid() {
		return btid;
	}

	public void setBtid(String btid) {
		this.btid = btid;
	}

	public Float getMainTemp() {
		return mainTemp;
	}

	public void setMainTemp(Float mainTemp) {
		this.mainTemp = mainTemp;
	}

	public Float getRemoteTemp() {
		return remoteTemp;
	}

	public void setRemoteTemp(Float remoteTemp) {
		this.remoteTemp = remoteTemp;
	}

	public Float getRemoteHumidity() {
		return remoteHumidity;
	}

	public void setRemoteHumidity(Float remoteHumidity) {
		this.remoteHumidity = remoteHumidity;
	}

	public Float getRemoteLight() {
		return remoteLight;
	}

	public void setRemoteLight(Float remoteLight) {
		this.remoteLight = remoteLight;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getMealType() {
		return mealType;
	}

	public void setMealType(String mealType) {
		this.mealType = mealType;
	}

	public String getOpen() {
		return open;
	}

	public void setOpen(String open) {
		this.open = open;
	}

	public String getPlugState() {
		return plugState;
	}

	public void setPlugState(String plugState) {
		this.plugState = plugState;
	}

	public String getFanState() {
		return fanState;
	}

	public void setFanState(String fanState) {
		this.fanState = fanState;
	}

	public String getRemoteSensor() {
		return remoteSensor;
	}

	public void setRemoteSensor(String remoteSensor) {
		this.remoteSensor = remoteSensor;
	}

	public String getHeaterState() {
		return heaterState;
	}

	public void setHeaterState(String heaterState) {
		this.heaterState = heaterState;
	}

	public String getLidState() {
		return lidState;
	}

	public void setLidState(String lidState) {
		this.lidState = lidState;
	}

	public Float getMainBatery() {
		return mainBatery;
	}
	
	public Integer getMainBateryInt() {
		return Math.round(mainBatery);
	}

	public void setMainBatery(Float mainBatery) {
		this.mainBatery = mainBatery;
	}

	public boolean isProcessed() {
		return processed;
	}

	public void setProcessed(boolean processed) {
		this.processed = processed;
	}

	@Override
	public String toString() {
		return "BagTagDataDTO [time=" + time + ", btid=" + btid + ", mainTemp="
				+ mainTemp + ", remoteTemp=" + remoteTemp + ", remoteHumidity="
				+ remoteHumidity + ", remoteLight=" + remoteLight + ", mode="
				+ mode + ", mealType=" + mealType + ", open=" + open
				+ ", plugState=" + plugState + ", fanState=" + fanState
				+ ", remoteSensor=" + remoteSensor + ", heaterState="
				+ heaterState + ", lidState=" + lidState + ", mainBatery="
				+ mainBatery + ", processed=" + processed + "]";
	}

	

	
}
